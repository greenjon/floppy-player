# Floppy Player

A jukebox made with an ARM SBC running Armbian, mpd and mpc. Oh, and a 3.5" USB floppy drive.

When a floppy disk is inserted into the drive, tracks get automatically loaded into mpd's queue and start playing. Currently tested methods of populating the queue are by loading a playlist (example: load the playlist called 'Blues'), or by using an algorithm (example: load all tracks with the Genre tag of 'Blues").

This concept has long seemed interesting to me but I didn't have the necessary skills to begin it until I saw the Diskplayer project: https://www.dinofizzotti.com/blog/2020-02-05-diskplayer-using-3.5-floppy-disks-to-play-albums-on-spotify/ From this project I learned how to make a script automatically run when a floppy is inserted or removed.

Installation sequence:

Install Armbian or another OS (I used one with a desktop but this could be done on a server I suppose).

Install mpd and mpc.

Make a script run when a floppy is inserted or removed:
- Attach the USB floppy drive.
- Run lsusb from terminal. Note the ID of the floppy which will look like: ID 1234:ABCD. The first four digits are for the Vendor and the second four digits are for the Product.
- Make a udev rule using that ID. 
- Create /etc/udev/rules.d/100-floppy-change.rules and write this in it:
- ACTION=="change", ATTRS{idVendor}=="1234", ATTRS{idProduct}=="ABCD", ENV{DISK_MEDIA_CHANGE}=="1", RUN+="/home/username/floppy.sh $env{DEVNAME}"
- You'll have to change idVendor and idProduct to match the ID of your floppy. You'll also have to replace 'username' with your username.

The floppy.sh script NEEDSWORK

Making mpd start automatically NEEDSWORK

Buttons.py script NEEDSWORK

Set environtmental variables
- add MPD_HOST=127.0.0.1 and MPD_PORT=6969 to etc/environment


